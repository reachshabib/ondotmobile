// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic','ngCordova', 'ionic.service.core','ionic.service.push', 'app.controllers', 'app.routes', 'app.services', 'app.directives','onezone-datepicker'])

.run(function($ionicPlatform,$rootScope,$ionicHistory) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $rootScope.baseurl = "http://app.ondotfreight.com";
    //$rootScope.baseurl = "http://ondotapp.cloudapp.net";
  });

    $ionicPlatform.onHardwareBackButton(function() {
      $ionicHistory.goBack();
    });
}).run(function($ionicPlatform, $window, $rootScope,$state,$timeout,shipments,userMeta) {

  $ionicPlatform.ready(function() {

    var push = new Ionic.Push({
      "debug": true,
      "onNotification": function(notification) {
        var payload = notification.payload;
        console.log(notification, payload);
        $state.go("app.shipmentDetails",{"shipmentId":payload.shipmentid})
      }
    });


    push.register(function (token){
      console.log("Device token:",token.token);
      $window.localStorage["device_token"]= token.token;
    })

  });
    //$timeout(function() {
    //    shipments.getTimelyShipments();
    //}, 1000);
    userMeta.loadData();
}).config(function($ionicConfigProvider) {
$ionicConfigProvider.tabs.position("bottom"); //Places them at the bottom for all OS
$ionicConfigProvider.tabs.style("standard"); //Makes them all look the same across all OS
     $ionicConfigProvider.scrolling.jsScrolling(false);
}).constant('configOndot',{
  "BASE_URL": "http://onder.southeastasia.cloudapp.azure.com",
  "SUPPORT_EMAIL": "http://onder.southeastasia.cloudapp.azure.com",
})
