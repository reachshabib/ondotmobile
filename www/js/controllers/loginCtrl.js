angular.module('app')

  .controller('loginCtrl', function ($scope, $ionicPush, $localstorage, $http, $state, $rootScope, factoryxhr, $ionicHistory, userMeta) {
    $ionicHistory.clearHistory();
    $scope.auth = {};
    $scope.errmsg = {};

    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = false;
    });
    $scope.signIn = function () {
      var url = '/api/v1/auth/login';
      var postData = {
        "email": $scope.auth.email.toLowerCase(),
        "password": $scope.auth.password,
        "device_token": $localstorage.get("device_token")
      }

      factoryxhr.asyncXHR(1, url, postData, $scope.loginSuccessCallback, null, $scope.loginFailureCallback);
    }


    $scope.loginSuccessCallback = function (data) {
      if (data.success) {
        $localstorage.set("authToken", data.auth_token);
        $localstorage.setObject("user", data);
        userMeta.loadData();
        $scope.errmsg.message = "";
        console.log("Going to dashboard");
        $state.go('app.tabs.dashboard');

      }
      else {
        if (data.errors) {
          console.log(data.errors[0].message);
          $scope.errmsg.message = data.errors[0].message;
        }
      }
    }

    $scope.loginFailureCallback = function () {
      $scope.errmsg.message = "Make sure you are connected to internet";
    }
    $scope.init = function () {

      var url = '/api/v1/auth/info';
      var postData = {
        "device_token": $localstorage.get("device_token")
      };
      factoryxhr.asyncXHR(1, url, postData, $scope.userInfoCallback);
      userMeta.loadData();
      if (userMeta.loggedIn) {
        $state.go('app.tabs.dashboard');
      }

    }



    $scope.logoutSuccessCallback = function () {
      $state.go("app.login");
    }
    $scope.loginFailureCallback = function () {
      $state.go("app.login");
    }

    $scope.init();
  })
