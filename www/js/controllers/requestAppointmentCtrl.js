angular.module('app')

  .controller('requestAppointmentCtrl', function ($scope, $localstorage, factoryxhr, $state, $rootScope,$cordovaToast,configOndot,userMeta) {
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.isRequestComplete = true;

    $scope.onezoneDatepicker = {
      date: new Date() // MANDATORY
    };
    $scope.requestAppointment = function () {
      //var url = '/request/appointment';
      var url = '/api/v1/message/email';
      var postData = {
        "to_email":configOndot["SUPPORT_EMAIL"],
        "to_name":"Ondot Freight Support",
        "message":"Request For appointment on "+$scope.onezoneDatepicker.date +"",
        "subject":"Request For appointment on "+$scope.onezoneDatepicker.date +"",
        //"date": $scope.onezoneDatepicker.date +"",
        //"device_token": $localstorage.get("device_token")
      }
      console.log(postData.date);
      factoryxhr.asyncXHR(1, url, postData, $scope.appointmentSuccessCallback, null,$scope.requestFailed);
      $scope.isRequestComplete = false;
    }


    $scope.appointmentSuccessCallback = function (data) {
      console.log("Appointment Successful");
      $scope.isRequestComplete = true;
      $state.go("app.thankYou", {message: data.message});
    }

    $scope.init = function () {
    }

    $scope.requestFailed = function () {
      $cordovaToast.showShortBottom('Please Connect to Internet').then(function (success) {
        // success
      }, function (error) {
        // error
      });
    }
    $scope.init();
  })
