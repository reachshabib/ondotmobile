angular.module('app.controllers', [])


  .controller('thankYouCtrl', function ($scope, $stateParams) {
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.thankyou = {};
    $scope.init = function () {
      $scope.thankyou.message = $stateParams.message;
    }
    $scope.init();
  })

  .controller('forgotCtrl', function ($scope, $stateParams, factoryxhr, $cordovaToast) {
    $scope.auth = {};
    $scope.errmsg = {};
      $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.resetPassword = function () {
      var url = '/api/v1/auth/forgot';
      var postData = {
        "email": $scope.auth.email
      }

      factoryxhr.asyncXHR(1, url, postData, $scope.resetSuccessCallback, null, $scope.resetFailureCallback);
    }


    $scope.resetSuccessCallback = function (data) {
      console.log("Password resetted here");
      console.log(data.success);
      $scope.errmsg.message = "Password reset message been sent to your email";
    }

    $scope.resetFailureCallback = function () {
      $scope.errmsg.message = "Password reset Failed, Please Check the network";
      $cordovaToast.showShortBottom('Please Connect to Internet').then(function (success) {
        // success
      }, function (error) {
        // error
      });

    }

  })

  .controller('AppController', function ($scope,$localstorage,$state, $ionicSideMenuDelegate,userMeta,factoryxhr,shipments) {
    $scope.userMeta = userMeta;
    $scope.date = new Date();
    $scope.toggleLeft = function () {
      $ionicSideMenuDelegate.toggleLeft();
    }
    $scope.logout = function () {
      var url = '/api/v1/auth/logout';
      $localstorage.delete("user");
      $localstorage.delete("authToken");
      $localstorage.clearAllShipments();
      userMeta.clear();
      factoryxhr.asyncXHR(1, url, {"device_token": $localstorage.get("device_token")}, $scope.logoutSuccessCallback, null, $scope.loginFailureCallback);
    }
     $scope.logoutSuccessCallback = function () {
      $state.go("app.login");
     }
     $scope.loginFailureCallback = function () {
      $state.go("app.login");
     }
  })
