angular.module('app')

  .controller('requestSpecialCtrl', function ($scope, $rootScope, $state, factoryxhr, $localstorage, $cordovaToast,$ionicPopup,configOndot) {
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.special = {};

    $scope.requestStatusReport = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Request Call',
        template: 'Are you sure ?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          //var url = '/request/status';
          var url = '/api/v1/message/email';
          var postData = {
            "to_email":configOndot["SUPPORT_EMAIL"],
            "to_name":"Ondot Freight Support",
            "message":"Requesting Status Report",
            "subject":"Requesting Status Report",
          }
          factoryxhr.asyncXHR(1, url, postData, $scope.customerRequestCallback, null, $scope.requestFailed);
        } else {
          console.log('You are not sure');
        }
      });
    }

    $scope.requestSOA = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Request Call',
        template: 'Are you sure ?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          //var url = '/request/soa';
          var url = '/api/v1/message/email';
          var postData = {
            "to_email":configOndot["SUPPORT_EMAIL"],
            "to_name":"Ondot Freight Support",
            "message":"Requesting SOA",
            "subject":"Requesting SOA",
          }
          factoryxhr.asyncXHR(1, url, postData, $scope.customerRequestCallback, null, $scope.requestFailed);
        } else {
          console.log('You are not sure');
        }
      });
    }

    $scope.customerRequestCallback = function (data) {
      $state.go("app.thankYou", {message: data.message});
    }

    $scope.init = function () {

    }

    $scope.requestFailed = function () {
      $cordovaToast.showLongBottom('Please Connect to Internet').then(function (success) {
        // success
      }, function (error) {
        // error
      });
    }
    $scope.init();
  })
