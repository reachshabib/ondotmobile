angular.module('app')

  .controller('accountCtrl', function ($scope, $localstorage, $state, $rootScope,$ionicHistory,factoryxhr) {
    $ionicHistory.clearHistory();
    $scope.logoutUser = function () {
      var url = 'api/v1/auth/logout';
      $localstorage.delete("user");
      $localstorage.delete("authToken");
      $localstorage.clearAllShipments();
      factoryxhr.asyncXHR(1, url, {"device_token": $localstorage.get("device_token")}, $scope.logoutSuccessCallback, null, $scope.loginFailureCallback);
    }
     $scope.logoutSuccessCallback = function () {
      $state.go("login");
     }
     $scope.loginFailureCallback = function () {
      $state.go("login");
     }
  })
