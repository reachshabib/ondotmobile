angular.module('app')
  .controller('dashboardCtrl', function ($scope, $ionicHistory, $localstorage, $rootScope, factoryxhr,$state,$cordovaToast,userMeta,shipments,$ionicPopup,configOndot) {

    $ionicHistory.clearHistory();

    $scope.userMeta = userMeta;
    $scope.shipments = shipments;
    $scope.arrivalupdates = false;

    $scope.init = function () {
      console.log("Getting the shipments details");
      shipments.getShipments();
    }

    $scope.requestCallFromUs = function () {
     var confirmPopup = $ionicPopup.confirm({
       title: 'Request Call',
       template: 'Are you sure ?'
     });

     confirmPopup.then(function(res) {
       if(res) {
          //var url = '/request/call';
         var url = '/api/v1/message/email';
         var postData = {
           "to_email":configOndot["SUPPORT_EMAIL"],
           "to_name":"Ondot Freight Support",
           "message":"Requesting Call",
           "subject":"Requesting Call",
         }
          factoryxhr.asyncXHR(1, url, postData, $scope.callSuccessCallback, null,$scope.requestFailed);
       } else {
         console.log('You are not sure');
       }
     });

    }

    $scope.callSuccessCallback = function (data) {
      console.log("Call Request Successful Successful");
      //Navigate to thank you page and Tell them we will get back to you via email shortly
      $state.go("app.thankYou", {message: data.message});
    }

    $scope.requestFailed = function () {
      $cordovaToast.showShortBottom('Please Connect to Internet').then(function (success) {
        // success
      }, function (error) {
        // error
      });
    }
    $scope.init();

  })
