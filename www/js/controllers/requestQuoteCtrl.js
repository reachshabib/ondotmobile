angular.module('app')


  .controller('requestQuoteCtrl', function ($scope, $rootScope, $localstorage, factoryxhr,$state,configOndot) {

    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.quote = {};
    $scope.requestQuote = function () {
      //var url = '/request/quotation';
      var url = '/api/v1/message/email';
      if ($scope.quote.terms == 'others') {
        $scope.quote.terms = $scope.quote.termsother;
      }

      var message = "";
      message += "<div> Mode: "+ $scope.quote.mode + "</div><br>";
      message += "<div> Terms: "+ $scope.quote.terms + "</div><br>";
      message += "<div> Volume: "+ $scope.quote.volume + "</div><br>";
      message += "<div> Description: "+ $scope.quote.description + "</div><br>";
      message += "<div> Pickup Location: "+ $scope.quote.pickup_location + "</div><br>";
      message += "<div> POD: "+ $scope.quote.port_of_discharge + "</div><br>";
      var postData = {
        "to_email":configOndot["SUPPORT_EMAIL"],
        "to_name":"Ondot Freight Support",
        "message":message,
        "subject": "Requesting Quotation",
      }
      console.log(postData);
      factoryxhr.asyncXHR(1, url, postData, $scope.quoteSuccessCallback, null,$scope.requestFailed);
    }


    $scope.quoteSuccessCallback = function (data) {
      console.log("Quotation Successful");
      //Navigate to thank you page and Tell them we will get back to you via email shortly
      $state.go("app.thankYou", {message: data.message});
    }

    $scope.requestFailed = function () {
      $cordovaToast.showLongBottom('Please Connect to Internet').then(function (success) {
        // success
      }, function (error) {
        // error
      });
    }
  })
