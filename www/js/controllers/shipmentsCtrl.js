angular.module('app')

    .controller('shipmentsCtrl', function ($scope, $timeout, $rootScope, factoryxhr,$localstorage, $state, $ionicHistory,shipments) {
        $ionicHistory.clearHistory();
        $scope.shipments = shipments;
        $scope.init = function () {
            console.log("Getting the shipments details");
            shipments.getShipments();
        }

        $scope.doRefresh = function () {
            shipments.getShipments();
        }

        $scope.details = function (shipment) {
            console.log("Going to shipment details ");
          console.log(shipment);
            $state.go('app.shipmentDetails', {
                shipment: shipment,
                shipmentId: shipment._id.$oid
            });
        }
        $scope.init();
    })
