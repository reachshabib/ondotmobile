angular.module('app')
  .controller('shipmentDetailsCtrl', function ($scope, $rootScope, $stateParams, factoryxhr, $state, $localstorage, $cordovaToast, $ionicPopup) {
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.shipment = {};
    $scope.init = function () {

      $scope.newtworkError = null;
      console.log("Getting the shipments details")
      if ($stateParams.shipment) {
        $scope.shipment = $stateParams.shipment;
      } else {
        //Load the details seperately
        console.log("Shipment not forwarded by the Previous page, opening it directly");
        $scope.getShipment();
      }
      //var url = "/shipment/conversation/" + $stateParams.shipmentId + "/push";
      //factoryxhr.asyncXHR(0, url, {}, $scope.shipmentPushUpdateCallback, null, $scope.shipmentPushUpdateFailureCallback);
    };

    $scope.shipment = {};

    $scope.doRefresh = function () {
      //Fetch all the shipments again !!
      $scope.init();
    };
    $scope.getShipment = function () {
      $scope.shipment = $localstorage.getObject("shipment_" + $stateParams.shipmentId + "", {});
      var url = "/api/v1/multiview/shipments/" + $stateParams.shipmentId;
      factoryxhr.asyncXHR(0, url, {}, $scope.shipmentCallback, null, $scope.shipmentsFailureCallback);

    };

    $scope.shipmentCallback = function (data) {
      if (data.success) {
        $localstorage.setObject("shipment_" + $stateParams.shipmentId + "", data.data);
        $scope.shipment = data.data;
        console.log("shipment updated");
        console.log($scope.shipment);
      }
    };

    $scope.shipmentsFailureCallback = function (data) {
      var shipment = $localstorage.getObject("shipment_" + $stateParams.shipmentId + "");
      if (shipment) {
        $scope.shipment = shipment;
      } else {
        console.log("Internet Not available, throw an error");
        $cordovaToast.showShortBottom('Please Connect to Internet').then(function (success) {
          // success
        }, function (error) {
          // error
        });
      }
    }
    $scope.processUpdates = function(data)
    {
      var newUpdates = [];
      for(var i = 0 ; i < data.data.length;i++)
      {
        var update = {};
        var datearr = data.data[i][0].split(" ");
        update.content = data.data[i][1];
        update.day = datearr[1];
        update.month = datearr[2].substring(0,3).toUpperCase();
        update.year = datearr[3];
        update.time = datearr[5];
        newUpdates.push(update)
      }

      console.log(newUpdates);
      $scope.updates = newUpdates;
    }

    $scope.shipmentPushUpdateCallback = function (data) {
      console.log(data);
      $scope.processUpdates(data)

      $localstorage.setObject("shipment_" + $stateParams.shipmentId + "_updates", data);

      $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.shipmentPushUpdateFailureCallback = function (data) {
      var updates = $localstorage.getObject("shipment_" + $stateParams.shipmentId + "_updates");
      if (updates) {
        $scope.updates = updates;

      } else {
        console.log("Internet Not available, throw an error");
        $cordovaToast.showLongBottom('Please Connect to Internet').then(function (success) {
          // success
        }, function (error) {
          // error
        });
      }
    };


    $scope.requestInvoice = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Request Invoice',
        template: 'Are you sure ?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          //var url = '/request/invoice';
          var url = '/api/v1/message/email';
          var postData = {
            "to_email":configOndot["SUPPORT_EMAIL"],
            "to_name":"Ondot Freight Support",
            "message":"Requesting Invoice Job ID :" +$scope.shipment.shipment_id ,
            "subject":"Requesting Invoice Job ID :" + $scope.shipment.shipment_id,
          }
          factoryxhr.asyncXHR(1, url, postData, $scope.customerRequestCallback, null, $scope.requestFailed);
        } else {
          console.log('You are not sure');
        }
      });

    };

    $scope.requestAWBMBL = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Request AWB/MBL',
        template: 'Are you sure ?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          //var url = '/request/awb';
          var url = '/api/v1/message/email';
          var postData = {
            "to_email":configOndot["SUPPORT_EMAIL"],
            "to_name":"Ondot Freight Support",
            "message":"Requesting AWB for Job ID :" +$scope.shipment.shipment_id ,
            "subject":"Requesting AWB for Job ID :" + $scope.shipment.shipment_id,
          }
          factoryxhr.asyncXHR(1, url, postData, $scope.customerRequestCallback, null, $scope.requestFailed);
        } else {
          console.log('You are not sure');
        }
      });
    };

    $scope.customerRequestCallback = function (data) {
      $state.go("app.thankYou", {message: data.message});
    };

    $scope.requestFailed = function () {
      $cordovaToast.showLongBottom('Please Connect to Internet').then(function (success) {
        // success
      }, function (error) {
        // error
      });
    };

    $scope.init();
  });
