'use strict';

/**
 * @ngdoc factory
 * @name simplyMoveInApp.message
 * @description
 * # message
 * Service in the simplyMoveInApp.
 */
angular.module('app')
  .factory('userMeta', function ($localstorage) {
    var info = {};

    info.setName = function (name) {
      info.name = name;
    };
    info.setEmail = function (email) {
      info.email = email;
    };
    info.setPhone = function (phone) {
      info.phone = phone;
    };
    info.setRank = function (rank) {
      info.rank = rank;
    };
    info.setLoggedIn = function (loggedIn) {
      info.loggedIn = loggedIn;
    };

    info.loadData = function () {
      var data = $localstorage.getObject("user", null);
      if (data.username) {
        info.setName(data.username);
        info.setPhone(data.phone_no);
        info.setEmail(data.email);
        info.setLoggedIn(true);
      }
    }

    info.clear = function () {
      info.name = null;
      info.email = null;
      info.phone = null;
      info.loggedIn = false;
    }
    return info;
  });
