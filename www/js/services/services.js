angular.module('app.services', [])

  .factory('$localstorage', ['$window', function ($window) {
    return {
      set: function (key, value) {
        $window.localStorage[key] = value;
      },
      get: function (key, defaultValue) {
        return $window.localStorage[key] || defaultValue;
      },
      setObject: function (key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function (key) {
        return JSON.parse($window.localStorage[key] || '{}');
      },
      delete : function(key){
        delete $window.localStorage[key];
      },
      clearAllShipments : function(){
        for(key in $window.localStorage) {
          console.log((key+"").substring(0,8));
          if ((key+"").substring(0,8) == 'shipment'){
            delete $window.localStorage[key];
          }
        }
      }
    }
  }])

  .factory('factoryxhr', function ($http, $location, $rootScope, $localstorage, $timeout,configOndot) {
    // Service logic
    // ...

    var data = null;

    // Public API here
    return {
      asyncXHR: function (method, url, postData, callback, userData, failcallback) {
        var url = configOndot["BASE_URL"] + url;
        if (method === 0) {
          $http({
            url: url,
            method: 'GET',
            dataType: 'json',
            params: postData,
            headers: {
              "authToken": $localstorage.get("authToken")
            }
          }).success(function (data, status) {
              if (data.status === 403) {
                $location.path('#/');

              } else {
                if (callback){
                callback(data, status, userData);
                }
              }
            })
            .error(function (data) {
              if (failcallback)
                failcallback(data);
              //$rootScope.notification('OOPS!!! Something went wrong. Please try again after sometime.', 'error');
            });
        } else if (method === 1) {
          $http({
            url: url,
            method: 'POST',
            dataType: 'json',
            data: postData,
            headers: {
              "authToken": $localstorage.get("authToken")
            }
          }).success(function (data, status, header) {
              if (data.status === 403) {
                $location.path('#/');

              } else {
                if (callback){
                callback(data, status, userData);
                }
              }
            }).error(function (data) {
              console.log(data)
              if (failcallback)
                failcallback(data);
              //$rootScope.notification('OOPS!!! Something went wrong. Please try again after sometime.', 'error');
            });
        } else if (method === 2) {
          $http({
            url: url,
            method: 'PUT',
            dataType: 'json',
            data: postData,
            headers: {
              "authToken": $localstorage.get("authToken")

            }
          }).success(function (data, status) {
              if (data.status === 403) {
                $location.path('#/');

              } else {
                if (callback){
                callback(data, status, userData);
                }
              }
            })
            .error(function (data) {
              if (failcallback)
                failcallback(data);
              //$rootScope.notification('OOPS!!! Something went wrong. Please try again after sometime.', 'error');
            });
        }
      }
    };
  })
  .filter('capitalize', [function () {
    return function (input, scope) {
      if (input != null)
        input = input.toLowerCase();
      return input.substring(0, 1).toUpperCase() + input.substring(1);
    };
  }])
  .filter('substitute', [function () {
    return function (text, find, replace) {
      var n = text.split(find);
      var r = n.join(replace);
      return r;
    };
  }])
;

