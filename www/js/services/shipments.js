'use strict';

/**
 * @ngdoc factory
 * @name simplyMoveInApp.message
 * @description
 * # message
 * Service in the simplyMoveInApp.
 */
angular.module('app')
  .factory('shipments', function (factoryxhr,$localstorage,$rootScope,$cordovaToast,$timeout) {
    var service = {};
    service.list = [];
    service.arrival = [];

    service.processArrival = function()
    {
      var arrivalNew = [];
      for(var i =0 ; i < service.list.length; i++)
      {
        if(service.list[i].shipment_status == 'transit' || service.list[i].shipment_status == 'arrived')
        {
          arrivalNew.push(service.list[i]);
        }
      }
      service.arrival = arrivalNew;
    }
    service.getShipments = function () {
      //load from local storage first, then initiate the api .
      service.list = $localstorage.getObject("shipments", {});
      service.processArrival();
      // Initiating the api
      var url = "/api/v1/multiview/shipments";
      factoryxhr.asyncXHR(0, url, {}, service.shipmentsCallback, null, service.shipmentsFailureCallback);
    }
    service.shipmentsCallback = function (data) {
      console.log(data);
      service.list = data.data;
      service.processArrival();
      $localstorage.setObject("shipments", data);
      $rootScope.$broadcast('scroll.refreshComplete');
    }

    service.shipmentsFailureCallback = function (data) {
      console.log(data);
      var shipments = $localstorage.getObject("shipments");
      if (shipments[0]) {
        service.list = shipments;
        service.processArrival();
      } else {
        $cordovaToast.showLongBottom('Please Connect to Internet').then(function (success) {
          // success
        }, function (error) {
          // error
        });
      }
      $rootScope.$broadcast('scroll.refreshComplete');
    }

    service.getTimelyShipments = function () {
      var url = "/api/v1/multiview/shipments";
      factoryxhr.asyncXHR(0, url, {}, timelyShipmentsCallback,null ,timelyShipmentsFailedCallback);
    }

    var timelyShipmentsCallback = function (data) {
      console.log(data);
      $localstorage.setObject("shipments",data);
      service.list = data;
      service.processArrival();
      $timeout(function() {
        service.getTimelyShipments();
    }, 60000);
    }

    var timelyShipmentsFailedCallback = function (data) {
      $timeout(function() {
        service.getTimelyShipments();
    }, 60000);
    }

    service.clear = function () {
      service.list = [];
    }
    return service;
  });
