angular.module('app.routes', [])

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $ionicConfigProvider.tabs.position('bottom');
    $stateProvider

      .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html"
      })

      .state('app.login', {
        url: '/login',
        views: {
          'appContent': {
            templateUrl: 'templates/login.html',
            controller: 'loginCtrl'}}
      })

      .state('app.forgot', {
        url: '/forgot',
        views: {
          'appContent': {
            templateUrl: 'templates/forgot-password.html',
            controller: 'forgotCtrl'
          }}
      })
      .state('app.tabs', {
        url: '/tab',
        views: {
          'appContent': {
            templateUrl: 'templates/tabs.html'
          }
        }
      })


      .state('app.tabs.dashboard', {
        url: '/dashboard',
        views: {
          'tab1': {
            templateUrl: 'templates/dashboard.html',
            controller: 'dashboardCtrl'
          }
        }
      })


      .state('app.tabs.shipments', {
        url: '/shipments',
        views: {
          'tab2': {
            templateUrl: 'templates/shipments.html',
            controller: 'shipmentsCtrl'
          }
        }
      })


      .state('app.tabs.account', {
        url: '/account',
        views: {
          'tab3': {
            templateUrl: 'templates/account.html',
            controller: 'accountCtrl'
          }
        }
      })


      .state('app.shipmentDetails', {
        url: '/shipment/detail?shipmentId',
        params: {
          shipment: null,
          shipmentId: null
        },
        views: {
          'appContent': {
            templateUrl: 'templates/shipmentDetails.html',
            controller: 'shipmentDetailsCtrl'}}
      })


      .state('app.requestAppointment', {
        url: '/requestAppointment',
        views: {
          'appContent': {
            templateUrl: 'templates/requestAppointment.html',
            controller: 'requestAppointmentCtrl'}}
      })


      .state('app.requestQuote', {
        url: '/requestQuote',
        views: {
          'appContent': {
            templateUrl: 'templates/requestQuote.html',
            controller: 'requestQuoteCtrl'
          }}
      })

      .state('app.requestSpecial', {
        url: '/requestSpecial',
        views: {
          'appContent': {
            templateUrl: 'templates/specialRequests.html',
            controller: 'requestSpecialCtrl'
          }}
      })

      .state('app.thankYou', {
        url: '/thankYou?message',
        params: {
          message: null
        },
        views: {
          'appContent': {
            templateUrl: 'templates/thankyou.html',
            controller: 'thankYouCtrl'
          }}
      })

    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');

  });